
export class LoadQueue {
  isLoading: boolean;
  private queue: string[] = [];

  push(queuer: string): void {
    this.queue.push(queuer);
    this.isLoading = this.queue.length > 0;
  }

  remove(queuer: string): void {
    const index = this.queue.indexOf(queuer, 0);
    if (index > -1) {
      this.queue.splice(index, 1);
    }
    this.isLoading = this.queue.length > 0;
  }
}
