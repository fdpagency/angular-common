import { ActivatedRoute, Params } from '@angular/router';

export class RouteHelper {

  public static findParam(param: string, route: ActivatedRoute): any {
    const routes: Array<ActivatedRoute> = [];
    let top: ActivatedRoute = route;
    while (top != null) {
      routes.push(top);
      top = top.parent;
    }
    let value: any = null;
    for (const i in routes) {
      if (routes.hasOwnProperty(i)) {
        routes[i].params.forEach((params: Params) => {
          if (params[param] !== undefined) {
            value = params[param];
          }
        });
        if (value != null) {
          break;
        }
      }
    }
    return value;
  }

  public static findQueryParam(param: string, route: ActivatedRoute): any {
    const routes: Array<ActivatedRoute> = [];
    let top: ActivatedRoute = route;
    while (top != null) {
      routes.push(top);
      top = top.parent;
    }
    let value: any = null;
    for (const i in routes) {
      if (routes.hasOwnProperty(i)) {
        routes[i].queryParams.forEach((params: Params) => {
          if (params[param] !== undefined) {
            value = params[param];
          }
        });
        if (value != null) {
          break;
        }
      }
    }
    return value;
  }

  public static findData(param: string, route: ActivatedRoute): any {
    const routes: Array<ActivatedRoute> = [];
    let top: ActivatedRoute = route;
    while (top != null) {
      routes.push(top);
      top = top.parent;
    }
    let value: any = null;
    for (const i in routes) {
      if (routes.hasOwnProperty(i)) {
        routes[i].data.forEach((params: Params) => {
          if (params[param] !== undefined) {
            value = params[param];
          }
        });
        if (value != null) {
          break;
        }
      }
    }
    return value;
  }

  public static parseQuery(query: string): { [id: string]: any; } {
    const params = {};
    const queries = query.split('&');
    for (let i = 0; i < queries.length; i++) {
      const temp = queries[i].split('=');
      params[temp[0]] = temp[1];
    }
    return params;
  }
}
